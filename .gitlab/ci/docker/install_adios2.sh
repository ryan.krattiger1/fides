#!/bin/sh

cd /tmp
mkdir -p Software

# Install ADIOS2
cd /tmp/Software
git clone https://github.com/ornladios/ADIOS2.git --single-branch --branch master
cd ADIOS2
git checkout $ADIOS_HASH_ENV
cd ..
mkdir -p ADIOS2-build
cd ADIOS2-build
if [ "$1" = "asan" ]; then
  cmake -GNinja \
    -DCMAKE_INSTALL_PREFIX=/opt/adios2 \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_CXX_FLAGS_DEBUG='-g -fsanitize=address -fsanitize=undefined' \
    -DADIOS2_BUILD_TESTING=OFF \
    -DADIOS2_BUILD_EXAMPLES=OFF \
    -DADIOS2_USE_Fortran=OFF \
    ../ADIOS2
else
  cmake -GNinja \
    -DCMAKE_INSTALL_PREFIX=/opt/adios2 \
    -DCMAKE_BUILD_TYPE=Debug \
    -DADIOS2_BUILD_TESTING=OFF \
    -DADIOS2_BUILD_EXAMPLES=OFF \
    -DADIOS2_USE_Fortran=OFF \
    ../ADIOS2
fi
ninja
ninja install

cd /tmp
rm -fr Software/
