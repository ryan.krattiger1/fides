#!/bin/bash

# VTK-m v2.0.0 - Feb 15, 2023
VTKM_HASH=cfd6d3fbe534343d217c2dce47a46763692db0ec
VTKM_SHORT_HASH=${VTKM_HASH:0:8}

# ADIOS2 master - Jan 25, 2023
ADIOS_HASH=289fabbc1c19f18ba00f3075b0782857514a412f
ADIOS_SHORT_HASH=${ADIOS_HASH:0:8}

cp install_cmake.sh ubuntu18
cp install_adios2.sh ubuntu18
cp install_vtkm.sh ubuntu18
IMAGE_NAME=kitware/vtk:ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}-latest
docker image build --build-arg VTKM_HASH=$VTKM_HASH \
  --build-arg ADIOS_HASH=$ADIOS_HASH \
  -t $IMAGE_NAME \
  ubuntu18/
rm ubuntu18/install_adios2.sh
rm ubuntu18/install_vtkm.sh
rm ubuntu18/install_cmake.sh
docker push $IMAGE_NAME

cp install_cmake.sh ubuntu18-ompi
cp install_adios2.sh ubuntu18-ompi
cp install_vtkm.sh ubuntu18-ompi
IMAGE_NAME=kitware/vtk:ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}-ompi-latest
docker image build --build-arg VTKM_HASH=$VTKM_HASH \
  --build-arg ADIOS_HASH=$ADIOS_HASH \
  -t $IMAGE_NAME \
  ubuntu18-ompi/
rm ubuntu18-ompi/install_adios2.sh
rm ubuntu18-ompi/install_vtkm.sh
rm ubuntu18-ompi/install_cmake.sh
docker push $IMAGE_NAME

#edit names in .gitlab-ci.yml
sed -i -e "s/ci-fides-vtkm[a-zA-Z0-9]\{8\}-adios[a-zA-Z0-9]\{8\}/ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}/g" ../../os-linux.yml
