#=========================================================================
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=========================================================================

# This code has been adapted from remus (https://gitlab.kitware.com/cmb/remus)

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR
   CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
  set(CMAKE_COMPILER_IS_CLANGXX 1)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_CLANGXX)

  #Add option for enabling sanitizers
  option(FIDES_ENABLE_SANITIZER "Build with sanitizer support." OFF)
  mark_as_advanced(FIDES_ENABLE_SANITIZER)

  if(FIDES_ENABLE_SANITIZER)
    set(FIDES_SANITIZER "address"
      CACHE STRING "The sanitizer to use")
    mark_as_advanced(FIDES_SANITIZER)

    if (UNIX AND NOT APPLE)
      if (FIDES_SANITIZER STREQUAL "address" OR
          FIDES_SANITIZER STREQUAL "undefined")
        find_library(FIDES_ASAN_LIBRARY
          NAMES libasan.so.6 libasan.so.5 libasan.so.4
          DOC "ASan library")
        mark_as_advanced(VTK_ASAN_LIBRARY)

        set(_fides_testing_ld_preload
          "${FIDES_ASAN_LIBRARY}")
      endif ()
    endif ()

    #We're setting the CXX flags and C flags beacuse they're propagated down
    #independent of build type.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=${FIDES_SANITIZER}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fsanitize=${FIDES_SANITIZER}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=${FIDES_SANITIZER}")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fsanitize=${FIDES_SANITIZER}")
  endif()
endif()

function (fides_sanitizer_apply_env)
  cmake_parse_arguments(PARSE_ARGV 0 _fides_sanitizer
    "ALL_IN_DIRECTORY"
    ""
    "TESTS")

  if (_fides_sanitizer_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR
      "Unrecognized arguments to `fides_sanitizer_apply_env`: ${_fides_sanitizer_UNPARSED_ARGUMENTS}")
  endif ()

  if (_fides_sanitizer_ALL_IN_DIRECTORY AND
      _fides_sanitizer_TESTS)
    message(FATAL_ERROR
      "`ALL_IN_DIRECTORY` conflicts with an explicit `TESTS` list")
  endif ()

  if (_fides_sanitizer_ALL_IN_DIRECTORY)
    get_property(_fides_sanitizer_TESTS
      DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
      PROPERTY TESTS)
  endif ()

  foreach (_fides_sanitizer_test IN LISTS _fides_sanitizer_TESTS)
    if (UNIX AND NOT APPLE)
      set_property(TEST "${_fides_sanitizer_test}" APPEND
        PROPERTY
          ENVIRONMENT "LD_PRELOAD=${_fides_testing_ld_preload}")
    endif ()
  endforeach ()
endfunction ()
