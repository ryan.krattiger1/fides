Fides 1.2.0 Release Notes
=========================

# Table of Contents
1. [Core](#Core)
    - Writer attribute output
    - Enable ADIOS source to be opened without MPI enabled
    - Deprecate `DataSetReader::ReadStep()`
    - `DataSource`: store most recent `StepStatus`
    - Add option for open or closed fusion mesh (GTC, XGC)
    - Support for ParaView Catalyst
    - Fix reading GTC arrays when planes are stored in different ADIOS blocks
    - Add Composite point array support
    - Add explicit `StreamingMode`
2. [Build](#Build)
    - Support added for VTK-m 1.9 and 2.0
    - Support for VTK-m built with CUDA
3. [Other](#Other)
    - Small changes/bug fixes


# Core

## Write attribute output

DataSetWriter now outputs the attributes Fides uses to generate the data model, so that
the ParaView Fides reader can read these files.

## Enable ADIOS source to be opened without MPI enabled

When Fides is built with MPI support, it can now choose whether to actually use MPI with ADIOS or not.
The static DataSetReader::CheckForDataModelAttribute() doesn't need to use MPI to simply check that an
ADIOS file can be read and will cause ParaView to hang if MPI is used.

## Deprecate `DataSetReader::ReadStep()`

Having both DataSetReader::ReadDataSet() (to be used in random access mode) and DataSetReader::ReadStep()
(used in streaming mode, in conjunction with DataSetReader::PrepareNextStep()) is error prone.
Since the only difference in these methods is whether they call DoAllReads or EndStep, the reader determines
if streaming mode is being used and makes the appropriate call for the mode in ReadDataSet() now.
ReadStep() is now deprecated.

## `DataSource`: store most recent `StepStatus`

In ADIOS, checks were added to the engines when using BeginStep/EndStep that were causing failures in Fides.
Most situations we test for were fine, but XGC has some DataSources that contain variables with time steps and
some do not. Since DataSetReader manages all DataSources together, it was trying to call BeginStep or EndStep
on sources that should not have it called. So DataSource now stores the most recent StepStatus, so once it hits
EndOfStream it will no longer call those functions on that source.

## Add option for open or closed fusion mesh (GTC, XGC)

Adds Key `FUSION_PERIODIC_CELLSET()` for specifying that mesh should be periodic for GTC and XGC

## Support for ParaView Catalyst

For Catalyst support, this adds the ability to specify an ADIOS variable as the time values for steps.
Also can pass the address to an `ADIOS::IO` object to Fides, which is necessary when using the inline engine.

## Fix reading GTC arrays when planes are stored in different ADIOS blocks

We now support GTC planes written to different ADIOS blocks when using `BLOCK_SELECTION` in the Fides metadata.
This is similar to how blocks are handled in XGC where Fides blocks don't end up being the same thing as ADIOS blocks.

## Add Composite point array support

Composite array type lets you define 3 arrays where the values are zipped together. In the JSON for the coordinate system,
set `"array_type": "composite"`

## Add explicit `StreamingMode`

Previously `StreamingMode` was an internal flag set when using `PrepareNextStep` for the first time. However this does not work
well with the new BP5 format. BP5 requires that you specify random access or streaming mode when opening the engine. When streaming,
you cannot access any variables or attributes before the first `BeginStep`.

# Build

## Support added for VTK-m 1.9 and 2.0

We will continue to support VTK-m 1.9, in addition to 2.0.

## Support for VTK-m built with CUDA

Fixed issues that prevented building Fides with VTK-m that was built with CUDA.

# Other

## Small changes/Bug Fixes

- `DataSetReader::CheckForDataModelAttribute()` to return false instead of throwing an error
- Add ability to look for `fides/schema` in ADIOS file in addition to attributes for generating data model
- Remove `writer_id` param for inline engine
- Fix in `CoordinateSystem::GetNumberOfBlocks` when mesh is static and when number of blocks changes each time step.
- Fix type bugs when VTK-m is built with 32-bit ids
